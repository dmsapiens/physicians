<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();
	}

    public function index()
    {
        $data = array();
        $this->template->build('popup',$data);
    }
    
    public function save()
    {
        if ($_GET['action'] == 1) {
            $this->load->helper('cookie');
            $cookie = array(
                'name'   => '1q_hide_popup',
                'value'  => '1',
                'expire' => '11186500'
            );
            
            set_cookie($cookie);
        }
        echo 1;
        exit;
    }
}