<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();
        $this->template->set_layout('baseTemplate');
	}

    public function index()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $data = array();

        $data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
        
        $ratinglist = $this->misc->getratinglist();
        foreach($ratinglist as $row) {
            $rating = 'rating'.$row['rating_no'];
    		$data[$rating] = $row['counts'];
    	}
        
        $data['satisfiedlist'] = $this->misc->getsatisfiedlist(array('5','4'),5,0);
        $data['total_satisfied'] = $this->misc->getsatisfiedlisttotal(array('5','4'));
        
        $data['dissatisfiedlist']=$this->misc->getsatisfiedlist(array('1','2','3'),5,0,true);
        $data['total_dissatisfied']=$this->misc->getsatisfiedlisttotal(array('1','2','3'));

        $this->load->helper('cookie');
        if (get_cookie('1q_hide_popup')) {
            $data['hide_popup'] = true;
        } else {
            $data['hide_popup'] = false;
        }

        $this->template->build('dashboard',$data);
    }
    
    public function markedAsContacted($rid)
	{
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
		$this->misc->markAsContacted($rid);
		redirect('dashboard/review-details/'.$rid);
	}
    
    public function archivereview($id = false)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$id) {
            redirect('/');
        }
        
        $data = array();
        
        if ($this->misc->archiveReview($id)) {
            $this->session->set_flashdata('success', 'Review has been archived');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong, please check the link and try again');
        }
        
        redirect('dashboard');
    }
    
    public function viewreviewdetail($id = false)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$id) {
            redirect('/');
        }
        
        $data = array();
        
        $data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
        
        $ratinglist = $this->misc->getratinglist();
        foreach($ratinglist as $row) {
            $rating = 'rating'.$row['rating_no'];
    		$data[$rating] = $row['counts'];
    	}
        
        $data['userdetails'] = $this->misc->getSatisfiedDetails($id);
        $data['entrydate'] = $this->misc->convertdatefrommysql($data['userdetails'][0]['entry_date']);
        $data['user'] = $data['userdetails'][0]['firstname'].' '.$data['userdetails'][0]['lastname'];
        $data['rating_id'] = $id;
        
		$this->template->build('review_detail',$data);
    }
    
}