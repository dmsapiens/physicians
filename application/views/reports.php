<div id="sumInfo">
   <div class="row filterBox nopad">
      <div class="col-md-12">
         <h2>Filter Results</h2>
      </div>
      <?php $formattributes = array('name' => 'reportform');
         echo form_open('reports/showreport',$formattributes); ?>
      <div class="col-md-4 col-sm-4">
         <label for="">Timeframe</label>
         <select name="timeframe" id="timeframe">
            <option value="all">All</option>
            <option value="today">Today</option>
            <option value="7">Last Week</option>
            <option value="30">Last Month</option>
            <option value="90">Last 3 Months</option>
            <option value="180">Last 6 Months</option>
            <option value="365">Last Year</option>
         </select>
         <script>
            <?php if($timeframe!='') { ?>
            	$("#timeframe").val('<?php echo $timeframe; ?>');
            <?php } ?>
         </script>
      </div>
      <!-- // col  -->
      <div class="col-md-4 col-sm-4">
         <label for="">From</label>
         <input type="text" name="fromdate" id="datepicker" placeholder="Pick a Date" value="<?php echo $fromdate; ?>" >
      </div>
      <!-- // col sm 6  -->
      <div class="col-md-4 col-sm-4">
         <label for="">To</label>
         <input type="text" name="todate" id="datepicker2" placeholder="Pick a Date" value="<?php echo $todate; ?>" >
      </div>
      <!-- // col sm 6  -->
   </div>
   <!-- // row  -->
   <div class="row filterBox nopad">
      <div class="col-md-4 col-sm-4">
         <label for="">Rating</label>
         <select name="rating" id="rating">
            <option value="all">All</option>
            <option value="5">Extremely Satisfied</option>
            <option value="4">Very Satisfied</option>
            <option value="3">Satisfied</option>
            <option value="2">Dissatisfied</option>
            <option value="1">Very Dissatisfied</option>
            <option value="0">Archived</option>
         </select>
         <script>
            <?php if($rating!='') { ?>
            	$("#rating").val('<?php echo $rating; ?>');
            <?php } ?>
         </script>
      </div>
      <!-- // col  -->
      <!-- // col sm 6  -->
      <div class="col-md-4 col-sm-4">
         <label for="">Format</label>
         <select name="format" id="format">
            <option value="pdf">PDF</option>
            <option value="xls">XLS</option>
            <option value="csv">CSV</option>
         </select>
         <script>
            <?php if($format!='') { ?>
            	$("#format").val('<?php echo $format; ?>');
            <?php } ?>
         </script>
      </div>
      
      <div class="col-md-4 col-sm-4">
         <label for="">Location</label>
         <select name="location" id="location">
            <option value="all">All Locations</option>
            <?php foreach ($loc as $l): ?>
                <option value="<?php echo $l['id']; ?>"><?php echo $l['name']; ?></option>
            <?php endforeach; ?>
         </select>
         <script>
            <?php if($location!='') { ?>
            	$("#location").val('<?php echo $location; ?>');
            <?php } ?>
         </script>
      </div>
      <!-- // col sm 6  -->
   </div>
   
   <?php if ($this->misc->getPerson() != ''): ?>
   <div class="row filterBox nopad">
        <div class="col-md-4 col-sm-4">
         <label for="doctor"><?php echo $this->misc->getPerson(); ?></label>
         <select name="doctor" id="doctor">
            <option value="">All</option>
            <?php foreach ($doctors as $doc): ?>
                <option value="<?php echo $doc['id']; ?>"><?php echo $doc['firstname'] . ' ' . $doc['lastname']; ?></option>
            <?php endforeach; ?>
         </select>
         <script>
            <?php if($doctor!='') { ?>
            	$("#doctor").val('<?php echo $doctor; ?>');
            <?php } ?>
         </script>
      </div>
      <div class="col-md-4 col-sm-4">
         <label for="patient">Patient</label>
         <select name="patient" id="patient">
            <option value="">All</option>
            <?php foreach ($patients as $patient1): ?>
                <option value="<?php echo $patient1['id']; ?>"><?php echo $patient1['firstname'] . ' ' . $patient1['lastname']; ?></option>
            <?php endforeach; ?>
         </select>
         <script>
            <?php if($patient!='') { ?>
            	$("#patient").val('<?php echo $patient; ?>');
            <?php } ?>
         </script>
      </div>
   </div>
   <?php endif; ?>
   <!-- // row  -->
   <div class="row filterBox nopad">
      <div class="col-md-4 col-md-offset-4">
         <a href="#" class="generateBtn"><img src="<?php echo base_url(); ?>assets/admin/img/ico/generate.png" alt="">Generate Report</a>
      </div>
   </div>
   </form>
   <!-- // row  -->
   <div class="clearfix"></div>
</div>
<!-- // sum info -->

<?php if($reportlist): ?>
<div class="contentWrapper row">
<div class="col-md-12">
<div class="panel panel-default">
   <div class="panel-heading"><img src="<?php echo base_url(); ?>assets/admin/img/ico/result.png" alt="">Results
   </div>
   <div class="panel-body">
      <div style="float: left; height:500px; overflow-y: scroll; width:100%;">
      <table class="table table-striped table-hover">
         <?php if($reportlist) {  $i=$starting_no+1; ?>
         <tr>
            <th scope="col">Full Name</th>
            <th scope="col">Phone</th>
            <th scope="col">Email</th>
            <th scope="col">Rating</th>
            <th scope="col">Location</th>
            <th scope="col">Date</th>
         </tr>
         <?php foreach($reportlist as $report) {  ?>
         <tr>
            <td><?php echo $report['firstname']; ?> <?php echo $report['lastname']; ?></td>
            <td><?php echo $report['phone_no']; ?></td>
            <td><a href="<?php echo ($report['emailid']) ? 'mailto:' . $report['emailid'] : 'javascript:void(0);'; ?>"><?php echo ($report['emailid']) ? $report['emailid'] : 'No email'; ?></a></td>
            <td><?php echo $report['ratingtext']; ?></td>
            <td><?php echo $report['locationName']; ?></td>
            <td><?php echo $report['date']; ?></td>
         </tr>
         <?php $i++; } ?>
         <tr class="totalSumTable">
            <td colspan="6" class="totalSumTable"><span>Total Reviews: <?php echo $total_rows; ?></span>   </td>
         </tr>
         <?php } else { echo "<tr><td colspan='6'>Sorry, No Records Found!</td></tr>"; } ?>         
      </table>
      </div>
      <?php if($reportlist) { ?>
      <footer id="contentFooter">
         <div class="row">
            <div class="col-md-5 col-sm-12">
            </div>
            <!-- // col md 6 -->
            <div class="col-md-7 col-sm-12" id="exportControls">
               <a href="javascript: void(0);" onclick="genratefile()" class="exportXLS">Export</a>
            </div>
         </div>
         <!-- // row -->
      </footer>
      <?php } ?>
   </div>
</div>
</div>
</div>
<?php else: ?>
    <?php if (!$default): ?>
        <div class="contentWrapper row">
        <div class="col-md-12">
        <div class="panel panel-default">
           <div class="panel-heading"><img src="<?php echo base_url(); ?>assets/admin/img/ico/result.png" alt="">Results
           </div>
           <div class="panel-body">
              <div style="float: left; height:500px; overflow-y: scroll; width:100%;">
              <table class="table table-striped table-hover">
                 <?php echo "<tr><td colspan='6'>Sorry, No Records Found!</td></tr>";  ?>         
              </table>
              </div>
           </div>
        </div>
        </div>
        </div>
    <?php endif; ?>
<?php endif; ?>

<script src="<?php echo base_url('assets/admin/js/jquery-ui.js') ?>"></script>
<script>

    $(document).ready(function() {
       $("#patient").select2(); 
        $("#doctor").select2();
        
        $('#location').on('change',function() {
            $.get('<?php echo base_url('reports/doctors'); ?>?id=' + $(this).val(),function(res) {
                $('#doctor').empty();
                $('#doctor').append(res);
                $("#doctor").select2();
            });
        });
    });

   $( "#datepicker" ).datepicker({
   	inline: true
   }).datepicker("setDate", new Date());
   $( "#datepicker2" ).datepicker({
   	inline: true
   }).datepicker("setDate", new Date());
   $(".generateBtn").on("click",function(){
   
   document.reportform.submit();
   });
   function genratefile()
   {
   	var format=document.getElementById('format').value;
   	switch(format)
   	{
   		case 'pdf':window.location.href='<?php echo base_url('reports/genratepdf'); ?>';
   		break;
   		case 'xls':window.location.href='<?php echo base_url('reports/exportXls'); ?>';
   		break;
   		case 'csv':window.location.href='<?php echo base_url('reports/genratecsv'); ?>';
   		break;
   	}
   
   }
</script>