<div id="sumInfo">
   <div class="sumBox">
      <a href="<?php echo base_url('extremly_satisfied'); ?>">
       <span class="greenCircle"><?php print $rating5; ?></span>
       <h6>Extremly Satisfied</h6>
       </a>
      </div>
      <div class="sumBox">
      <a href="<?php echo base_url('very_satisfied'); ?>">
       <span class="lightgreenCircle"><?php print $rating4; ?></span>
       <h6>Very Satisfied</h6>
       </a>
      </div>
   <div class="sumBox">
      <span class="blue"><?php echo $rating5+$rating4+$rating3; ?></span>
      <h6>Total</h6>
   </div>
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<?php echo form_open('allsatisfied',array('name' => 'filterform')); ?>
<div class="contentWrapper row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading">
            <img src="<?php echo base_url(); ?>assets/admin/img/ico/sat.png" alt="">5 and 4 Stars Reviews
            <select name="filterduration" id="filterduration" class="form-control" onchange="javascript: document.filterform.submit();">
               <option value="all">All</option>
               <option value="today">Today</option>
               <option value="7">Last Week</option>
               <option value="30">Last Month</option>
               <option value="90">Last 3 Months</option>
               <option value="180">Last 6 Months</option>
               <option value="365">Last Year</option>
            </select>
            <script>
               <?php if(isset($searchterm) && $searchterm != '') { ?>
               	$("#filterduration").val('<?php echo $searchterm; ?>');
               <?php } ?>
            </script>
         </div>
         <div class="panel-body">
            <?php if($satisfiedlist) { $i=1; ; ?>
            <table class="table table-striped table-hover">
               <tr>
                  <th scope="col">No</th>
                  <th scope="col">Full Name</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Rating</th>
                  <th scope="col">Location</th>
                  <th scope="col" class="text-center">Action</th>
               </tr>
               <?php foreach($satisfiedlist as $satisfied) {  ?>
               <tr>
                  <td scope="col"><?php print $i; ?></td>
                  <td scope="col"><?php print ucwords($satisfied['firstname'].' '.$satisfied['lastname']); ?></td>
                  <td scope="col"><?php print $satisfied['phone_no']; ?></td>
                  <td>
                  <?php switch($satisfied['rating'])
                    {
                    	case '5': print "Extremely Satisfied";
                    	break;
                    	case '4': print "Very Satisfied";
                    	break;
                    	case '3': print "Satisfied";
                    	break;
                    	default: print "Rating Problem";
                    }
                 ?>
                 </td>
                 <td>
                        <?php echo $satisfied['locationName']; ?>
                        <?php if ($this->misc->getPerson() != ''): ?>
                        <br />
                        (<?php echo $satisfied['dfname'] . ' ' . $satisfied['dflname']; ?>)
                        <?php endif; ?>
                     </td>
                  <td scope="col" class="text-center">
                     <a data-original-title="View Details" href="<?php echo base_url('dashboard/review-details/' . $satisfied['rating_id']); ?>" class="viewBtn" data-toggle="tooltip" data-placement="top" title="">View</a>
                     <span data-toggle="modal" data-target="#myModal">
                     <a data-original-title="Archive" href="#" class="removeBtn" data-toggle="tooltip" data-placement="top" title="" data-review-id='<?php print $satisfied['rating_id']; ?>'>Archive</a>
                     </span>
                  </td>
               </tr>
               <?php $i++; } ?>
            </table>
            <footer id="contentFooter">
               <div class="row">
                  <div class="col-md-5 col-sm-12">
                     <?php  if(isset($pagination)) { ?>
                     <?php	 echo $pagination;
                        } ?>
                  </div>
                  <!-- // col md 6 -->
                  <?php } else { echo "Sorry, No Records Found!"; } ?>
               </div>
               <!-- // row -->
            </footer>
         </div>
      </div>
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title" id="myModalLabel">Archive Review</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure you want to archive this review ?</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-primary" id="delete-user-link">Confirm</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>
<!-- // content wrapper -->
</form>
<script>
   $(".removeBtn").on("click",function(){
   
       $("#delete-user-link").attr("onclick", "deleteme("+$(this).data('review-id')+")");
       // show the modal
   });
   
   function deleteme(rid) {
        window.location.href='<?php echo base_url(); ?>/allsatisfied/archivereview/'+rid;
   }
</script>