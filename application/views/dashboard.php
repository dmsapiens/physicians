<div id="sumInfo">
   <div class="sumBox" style="width: 16.5% !important;">
      <a href="<?php echo base_url('extremly_satisfied'); ?>">
         <span class="greenCircle"><?php isset($rating5)? print $rating5:print 0;  ?></span>
         <h6>Extremely Satisfied</h6>
      </a>
   </div>
   <div class="sumBox" style="width: 16.5% !important;">
      <a href="<?php echo base_url('very_satisfied'); ?>">
         <span class="lightgreenCircle"><?php isset($rating4)? print $rating4:print 0;  ?></span>
         <h6>Very Satisfied</h6>
      </a>
   </div>
   <div class="sumBox" style="width: 16.5% !important;">
      <a href="<?php echo base_url('satisfied'); ?>">
         <span class="yellowCircle"><?php isset($rating3)? print $rating3:print 0;  ?></span>
         <h6>Satisfied</h6>
      </a>
   </div>
   <div class="sumBox" style="width: 16.5% !important;">
      <a href="<?php echo base_url('dissatisfied'); ?>">
         <span class="pinkCircle"><?php isset($rating2)?print $rating2:print 0;  ?></span>
         <h6>Dissatisfied</h6>
      </a>
   </div>
   <div class="sumBox" style="width: 16.5% !important;">
      <a href="<?php echo base_url('very_dissatisfied'); ?>">
         <span class="redCircle"><?php isset($rating1)?print $rating1:print 0;  ?></span>
         <h6>Very Dissatisfied</h6>
      </a>
   </div>
   <div class="sumBox" style="width: 16.5% !important;">
      <a href="javascript:void(0);">
         <span class="redCircle" style="border: 5px solid #00AA8A; color: #fff; background:#00AA8A; "><?php $nps = ($rating5+$rating4) - ($rating2 + $rating1); echo $nps; ?></span>
         <h6>Net Promoter Score</h6>
      </a>
   </div>
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<div class="contentWrapper row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading"><img src="<?php echo base_url('assets/admin/img/ico/sat.png'); ?>" alt=""/>Satisfied Reviews (Last 5)</div>
         <div class="panel-body">
            <table class="table table-striped table-hover ">
               <?php if(isset($satisfiedlist) && $satisfiedlist) { ?>
               <tbody>
                  <tr>
                     <th scope="col">Full Name</th>
                     <th scope="col">Phone</th>
                     <th scope="col">Rating</th>
                     <th scope="col">Location</th>
                     <th scope="col" class="text-center">Action</th>
                  </tr>
                  <?php foreach($satisfiedlist as $satisfied): ?>
                  <tr>
                     <td><?php print ucwords($satisfied['firstname']); ?> <?php print ucwords($satisfied['lastname']); ?></td>
                     <td><?php print $satisfied['phone_no']; ?></td>
                     <!--td><a href="mailto:<?php echo $satisfied['emailid']; ?>"><?php print $satisfied['emailid']; ?></a></td-->
                     <td>
                     <?php switch($satisfied['rating'])
                        {
                        	case '5': print "Extremely Satisfied";
                        	break;
                        	case '4': print "Very Satisfied";
                        	break;
                        	default: print "Rating Problem";
                        }
                     ?>
                     </td>
                     <td>
                        <?php echo $satisfied['locationName']; ?>
                        <?php if ($this->misc->getPerson() != ''): ?>
                        <br />
                        (<?php echo $satisfied['dfname'] . ' ' . $satisfied['dflname']; ?>)
                        <?php endif; ?>
                     </td>
                     
                     <td class="text-center">
                        <a data-original-title="View Details" href="<?php echo base_url('dashboard/review-details/' . $satisfied['rating_id']); ?>" class="viewBtn" data-toggle="tooltip" data-placement="top" title="">View</a>
                        <span data-toggle="modal" data-target="#myModal">
                        <a data-original-title="Archive" href="#" class="removeBtn" data-toggle="tooltip" data-placement="top" title="" data-review-id='<?php print $satisfied['rating_id']; ?>'>Remove</a>
                        </span>
                     </td>
                  </tr>
                  <?php endforeach; ?>
               </tbody>
            </table>
            <?php } else { ?>
                <p style="margin-bottom: 0 !important; padding-top:15px;">No review(s) found</p>
                </tbody>
            </table>
            <?php } ?>
            <?php if(isset($satisfiedlist) && $satisfiedlist) { ?>
            <hr/>
            <small class="noticeInfo">No. of Reviews for Last Month <strong><?php isset($total_satisfied)?print $total_satisfied:print 0;?></strong></small>
            <a href="<?php echo base_url('allsatisfied'); ?>" class="viewAll">View All</a>
            <?php } ?>
         </div>
      </div>
      
      <div class="panel panel-default">
       <div class="panel-heading"><img src="<?php echo base_url('assets/admin/img/ico/not.png'); ?>" alt="">Dissatisfied Reviews (Last 5)</div>
       <div class="panel-body">
          <table class="table table-striped table-hover ">
             <?php if(isset($dissatisfiedlist) && $dissatisfiedlist!=null) { ?>
             <tbody>
                <tr>
                   <th scope="col">Full Name</th>
                   <th scope="col">Phone</th>
                   <th scope="col">Rating</th>
                   <th scope="col">Location</th>
                   <th scope="col" class="text-center">Action</th>
                </tr>
                <?php  foreach($dissatisfiedlist as $dissatisfied) { ?>
                <tr>
                   <td><?php print ucwords($dissatisfied['firstname']); ?> <?php print ucwords($dissatisfied['lastname']); ?></td>
                   <td><?php print $dissatisfied['phone_no']; ?></td>
                   <td><?php switch($dissatisfied['rating'])
                      {
                        case '3': print "Satisfied";
                       	break;
                      	case '2': print "Dissatisfied";
                      	break;
                      	case '1': print "Very Dissatisfied";
                      	break;
                      }
                      ?></td>
                      <td>
                        <?php echo $dissatisfied['locationName']; ?>
                        <?php if ($this->misc->getPerson() != ''): ?>
                        <br />
                        (<?php echo $dissatisfied['dfname'] . ' ' . $dissatisfied['dflname']; ?>)
                        <?php endif; ?>
                     </td>
                   <td class="text-center">
                      <a data-original-title="View Details" href="<?php echo base_url('dashboard/review-details/' . $dissatisfied['rating_id']); ?>" class="viewBtn" data-toggle="tooltip" data-placement="top" title="">View</a>
                      <span data-toggle="modal" data-target="#myModal">
                      <a data-original-title="Archive Review" href="javascript:void(0);" class="removeBtn" data-toggle="tooltip" data-placement="top" title="" data-review-id='<?php print $dissatisfied['rating_id']; ?>'>Archive</a>
                      </span>
                   </td>
                </tr>
                <?php } ?>
             </tbody>
          </table>
          <?php }  else {?>
            <p style="margin-bottom: 0 !important; padding-top:15px;">No review(s) found</p>
            </tbody>
            </table>
          <?php } ?>
          <?php if(isset($dissatisfiedlist) && $dissatisfiedlist!=null) { ?>
          <hr/>
          <small class="noticeInfo">No. of Reviews for Last Month <strong><?php isset($total_dissatisfied)?print $total_dissatisfied: print 0; ?></strong></small>
          <a href="<?php echo base_url('alldissatisfied'); ?>" class="viewAll">View All</a>
          <?php } ?>
       </div>
    </div>
      
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title" id="myModalLabel">Archive Review</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure you want to archive this review ?</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-primary" id="delete-user-link">Yes</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>
<!-- // content wrapper -->
<script>
    $(".removeBtn").on("click",function(){
        $("#delete-user-link").attr("onclick", "deleteme("+$(this).data('review-id')+")");
    });
   
    function deleteme(rid) {
        window.location.href='<?php echo base_url('dashboard/archivereview'); ?>/' + rid;
    }
</script>

<?php if (!$hide_popup): ?>
<script>
    $(document).ready(function() {
        var url = 'https://www.youtube.com/watch?v=3zSqw-PbsYo?fs=1&amp;autoplay=1';
        $.fancybox({
            padding: 0,
            'autoScale'     : false,
            'transitionIn'  : 'none',
            'transitionOut' : 'none',
            'width'         : 'auto',
            'height'        : 450,
            'scrolling'   : 'no',
            'href'          : '<?php echo base_url('videos'); ?>',
            'type'          : 'ajax'
        });
        
        $(document).on('click','#closePopup',function() {
            if ($('#showPopup').is(':checked')) {
                var sel = 1;
            } else {
                var sel = 0;
            }
            $.get('<?php echo base_url('videos/save'); ?>?action=' + sel,function(e) {
                if (e == 1) {
                    $.fancybox.close();
                } else {
                    alert('Something went wrong, try again!');
                } 
            });
        });
    });
</script>
<?php endif; ?>