<div id="sumInfo">
   <div class="sumBox">
      <span class="darkBlue"><?php print (isset($total['admin']) ? $total['admin'] : '0' ); ?></span>
      <h6>Admins</h6>
   </div>
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<div class="contentWrapper row">
   <div class="col-md-12">
   
        <?php if(($this->session->flashdata('error'))):?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php endif;?>
        
        <?php if( $this->session->flashdata('msg') ):?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('msg');?>
            </div>
        <?php endif;?>
   
      <div class="panel panel-default">
         <?php
            $f_attributes = array('name' => 'filterusertype');
            echo  form_open('usermanagement',$f_attributes); ?>
         <div class="panel-heading">
            <img src="<?php echo base_url(); ?>assets/admin/img/ico/userprofile.png" alt="">Users
            <select class="form-control" name="selecttype" id="selecttype" onchange="javascript: document.filterusertype.submit();">
               <option value="all">All</option>
               <option value="admin">Admin</option>
               <!--option value="regularuser">Regular Users</option>
               <option value="report">Reports</option-->
            </select>
         </div>
         <script>
            <?php if(isset($searchterm)) {?>
            		$('#selecttype').val('<?php print $searchterm ; ?>');
            	<?php } else { ?>
            		$('#selecttype').val('all');
            	<?php } ?>
            	
         </script>
         <div class="panel-body">
            <table class="table table-striped table-hover">
               <?php if($userlist) {  	 ?>
               <tr>
                  <th scope="col">First Name</th>
                  <th scope="col">Last Name</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Email</th>
                  <th scope="col">Role</th>
                  <th scope="col" class="text-center">Action</th>
               </tr>
               <?php	if(isset($userlist)) {
                  $i=1;
                  foreach($userlist as $user) {    ?>
                  <?php if ($user['usertype'] == 'root') continue; ?>
               <tr>
                  <td><?php print $user['firstname']; ?> </td>
                  <td><?php print $user['lastname']; ?> </td>
                  <td><?php print $user['phoneno']; ?> </td>
                  <td><?php print $user['email']; ?> </td>
                  <td><?php
                     Switch($user['usertype'])
                     {
                     	case "admin":
                     		print "Admin";
                     		break;
                     	case "regularuser":
                     		print "Regular User";
                     		break;
                     	case "report":
                     		print "Report";
                     		break;
                     }
                     ?>
                  <td class="text-center">
                     <a href="<?php echo base_url(); ?>usermanagement/view/<?php print $user['id']; ?>" class="viewBtn" data-toggle="tooltip" data-placement="top" title="View Details">View</a>
                     <a href="<?php echo base_url(); ?>usermanagement/edit/<?php print $user['id']; ?>" class="editBtn2" data-toggle="tooltip" data-placement="top" title="Edit">Edit</a>
                     <?php $a = $this->session->userdata('logged_in'); if ($a['id'] != $user['id']): ?>
                     <span  data-toggle="modal" data-target="#myModal">
                     <a href="javascript:void(0);" class="removeBtn" data-toggle="tooltip" data-placement="top" title="Remove user" data-review-id='<?php print $user['id']; ?>' >Remove</a>
                     </span>
                     <?php endif; ?>
                  </td>
               </tr>
               <?php } } ?>
               <tr class="totalSumTable">
                  <td colspan="6" class="totalSumTable"><span><?php
                     if(isset($type))
                     {
                     	Switch($type)
                     			{
                     				case "admin":
                     					print "Total Admin : ".$total['admin'];
                     					break;
                     				case "regularuser":
                     					print "Total Regular User : ".$total['regularuser'];
                     					break;
                     				case "report":
                     					print "Total Report : ".$total['report'];
                     
                     			}
                     }
                     else {
                     print "Total User : ".($total['totalusers']);
                     }
                     	 ?></span>   </td>
               </tr>
               <?php } else { ?>
               <tr>
               <tr>Sorry, No Records Found!</td></tr>
               <?php } ?>
            </table>
            <footer id="contentFooter">
               <div class="row">
                  <div class="col-md-5 col-sm-12">
                     <?php  if(isset($pagination)) { ?>
                     <?php	 echo $pagination;
                        } ?>
                  </div>
                  <!-- // col md 6 -->
                  <?php if ($this->misc->check_if_root()) { ?>
                  <div class="col-md-7 col-sm-12" id="exportControls">
                     <a href="<?php echo base_url(); ?>usermanagement/add" class="exportXLS">Add User</a>
                  </div>
                  <?php } ?>
               </div>
               <!-- // col md 6 -->
         </div>
         <!-- // row -->
         </footer>
      </div>
   </div>
   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
               <h4 class="modal-title" id="myModalLabel">Remove User</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this user ?</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary"  id="delete-user-link">Confirm</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- // col md -->
</div>
</form>
<script>
   $(".removeBtn").on("click",function(){
   
   	$("#delete-user-link").attr("onclick", "deleteme("+$(this).data('review-id')+")");
   	// show the modal
   });
   
   function deleteme(rid)
   {
   
   	window.location.href='<?php echo base_url(); ?>/usermanagement/remove_user/'+rid;
   
   }
</script>