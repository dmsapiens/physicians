<div id="sumInfo">
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<div class="contentWrapper row">
   <div class="col-md-12">
   
    <?php if(($this->session->flashdata('error'))):?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php endif;?>
        
        <?php if( $this->session->flashdata('msg') ):?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('msg');?>
            </div>
        <?php endif;?>
   
      <div class="panel panel-default">
         <div class="panel-heading">List of Clients
            <select style="float: right !important;" class="form-control" name="users" id="users">
                <option value="">All</option>
                <?php $all = $this->db->query("SELECT * FROM sr_admincontrol ORDER BY id DESC"); ?>
                <?php if ($all): ?>
                    <?php $all = $all->result_array(); ?>
                    <?php foreach ($all as $r): ?>
                        <option value="<?php echo $r['id']; ?>-<?php echo $r['client_id']; ?>"><?php echo $r['firstname'].' '.$r['lastname']; ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
         </div>
         <div class="panel-body">
            <?php if($archivedratinglist) {  $i=$starting_no+1; ?>
            <table class="table table-striped table-hover">
               <tr>
                  <th scope="col">Logo</th>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col"></th>
               </tr>
               <?php foreach($archivedratinglist as $archivedrating) {  ?>
               <tr>
                  <td scope="col"><img class="img-responsive" width="120px" height="150px" src="<?php echo base_url('logos/' . $archivedrating['logo']); ?>"/></td>
                  <td scope="col"><?php echo $archivedrating['name']; ?></td>
                  <td scope="col"><a href="emailto:<?php echo $archivedrating['client_email']; ?>"><?php echo $archivedrating['client_email']; ?></a></td>
                  <td></td>
               </tr>
               <tr>
                  <td colspan="4"><strong>User Accounts for this client</strong></td>
               </tr>
               <?php $q = $this->db->query("SELECT * FROM sr_admincontrol WHERE client_id = ?",array($archivedrating['id'])); ?>
               <?php $all = $q->result_array(); ?>
               <?php if ($q->num_rows() > 0): ?>
               <?php foreach ($all as $r): ?>
                <tr>
                <td><?php echo $r['firstname'] . ' ' . $r['lastname']; ?></td>
                <td><?php echo $r['email']; ?></td>
                <td scope="col"><a onclick="return confirm('Are you sure you want to login as <?php echo $r['firstname']; ?> ?');" href="<?php echo base_url('root/login/'.$r['id'].'/'.$archivedrating['id']); ?>">Login As</a></td>
                <td></td>
                </tr>
               <?php endforeach; ?>
               <?php else: ?>
                <tr>
                    <td colspan="4">No accounts for this client could be found</td>
                </tr>
               <?php endif; ?>
               <?php $i++; } ?>
            </table>
            <footer id="contentFooter">
               <div class="row">
                  <div class="col-md-5 col-sm-12">
                     <?php  if(isset($pagination)) { ?>
                     <?php	 echo $pagination;
                        } ?>
                  </div>
                  <?php } else { echo "Sorry, No Records Found!"; } ?>
               </div>
               <!-- // row -->
            </footer>
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>
<script>

    $(document).ready(function() {
       $("#users").select2({
        placeholder: "Select user to login"
       }); 
       $('.select2-container--default').css('float','right');
       $('.select2-container--default').css('width','250px');
       
       $('#users').on('change',function() {
            var val = $(this).val();
            var t = val.split('-');
            
            var c = confirm('Are you sure you want to login as this user?');
            if (c) {
                window.location = '<?php echo base_url('root/login/'); ?>/' + t[0] + '/' + t[1];
            }
       })
    });
</script>