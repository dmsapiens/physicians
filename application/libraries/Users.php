<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users {
    
    private $CI;
    
    public function __construct()
    {
        $this->CI =& get_instance();
    }
    
    public function searchterm_handler($searchterm)
	{
		if($searchterm)
		{
			$this->CI->session->set_userdata('searchterm', $searchterm);
		}
		elseif($this->CI->session->userdata('searchterm'))
		{
			$searchterm = $this->CI->session->userdata('searchterm');
		}
		else
		{
			$searchterm = '';
		}
		return $searchterm;
	}
    
    public function getuserlisttotal($data)
	{
		if($data['searchterm']!='')
		{
			switch ($data['searchterm'])
			{
				case 'all' : 		break;
				case 'admin' : 		    $this->CI->db->where('usertype','admin'); break;
				case 'regularuser' : 	$this->CI->db->where('usertype','regularuser'); break;
				case 'report' : 		$this->CI->db->where('usertype','report'); break;
			
			}
		}
		
		$this->CI->db->where('is_delete',0);
        $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$query = $this->CI->db->get('sr_admincontrol');

		return $query->num_rows();
	}
    
    public function getuserlist($data,$per_page,$starting)
	{
		if($data['searchterm']!='')
		{
			switch ($data['searchterm'])
			{
				case 'all' : 				break;
				case 'admin' : 				$this->CI->db->where('usertype','admin'); break;
				case 'regularuser' : 		$this->CI->db->where('usertype','regularuser'); break;
				case 'report' : 			$this->CI->db->where('usertype','report'); break;

			}
		}

		$this->CI->db->where('is_delete',0);
        $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$query = $this->CI->db->get('sr_admincontrol',$per_page,$starting);
		return $query->result_array();
	}
    
    public function getusertypr($type)
	{
		$this->CI->db->order_by('firstname', 'asc');
		$this->CI->db->where(array('usertype'=>$type,'is_delete'=>0,'client_id'=>$this->CI->session->userdata['logged_in']['client_id']));
		$query = $this->CI->db->get('sr_admincontrol');
        return $query->result_array();
	}
    
	public function getusercount()
	{
		$this->CI->db->select('count(*) as counts,usertype', 'asc');
		$this->CI->db->group_by('usertype');
		$this->CI->db->where('is_delete',0);
        $this->CI->db->where('usertype','admin');
        $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$query = $this->CI->db->get('sr_admincontrol');
		return $query->result_array();
	}
	
	function insert_user($filename = false)
	{
		$phone_num = $this->CI->input->post('phoneno');
		$data = array(
    		'password' => md5($this->CI->input->post('password')),
    		'firstname' => $this->CI->input->post('firstname'),
    		'lastname' => $this->CI->input->post('lastname'),
    		'phoneno' => $phone_num,
    		'email' => $this->CI->input->post('email'),
    		'note' => $this->CI->input->post('note'),
    		'usertype' => $this->CI->input->post('usertype'),
    		'upload_image' => $filename,
    		'added_date'=>date('Y-m-d H:i:s'),
    		'last_update'=>date('Y-m-d H:i:s'),
            'client_id' => $this->CI->session->userdata['logged_in']['client_id']
		);
		$q = $this->CI->db->insert('sr_admincontrol',$data);
	}
	
	public function get_user($id)
	{
		$this->CI->db->where('id',$id);
		$query = $this->CI->db->get('sr_admincontrol');
		$this->CI->db->where('is_delete',0);
        $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
		return $query->row_array();
	}
	
	public function get_userview($id)
	{
		$this->CI->db->where('id',$id);
		$query = $this->CI->db->get('sr_admincontrol');
		$this->CI->db->where('is_delete',0);
        $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
		return $query->row_array();
	}
    
	public function update_user($filename = false)
	{
		
		$phone_num = $this->CI->input->post('phoneno');
		if($this->CI->input->post('password')=="")
		{
				$password = $this->CI->input->post('pswd');
		}
		else
		{
			$password = md5($this->CI->input->post('password'));
		}
	
		$id = $this->CI->input->post('cid');
		if (!empty($_FILES['upload_image']['name']))
		{
			$data = array(
    			'password' => $password,
    			'firstname' => $this->CI->input->post('firstname'),
    			'lastname' => $this->CI->input->post('lastname'),
    			'phoneno' => $phone_num,
    			'email' => $this->CI->input->post('email'),
    			'note' => $this->CI->input->post('note'),
    			'usertype' => $this->CI->input->post('usertype'),
    			'upload_image' => $filename,
    			'last_update'=>date('Y-m-d H:i:s'),
                'client_id' => $this->CI->session->userdata['logged_in']['client_id']
			);
		}
		else
		{
			$data = array(
    			'password' => $password,
    			'firstname' => $this->CI->input->post('firstname'),
    			'lastname' => $this->CI->input->post('lastname'),
    			'phoneno' => $phone_num,
    			'email' => $this->CI->input->post('email'),
    			'note' => $this->CI->input->post('note'),
    			'usertype' => $this->CI->input->post('usertype'),
    			'last_update'=>date('Y-m-d H:i:s'),
                'client_id' => $this->CI->session->userdata['logged_in']['client_id']
			);
		}
		$this->CI->db->where('id', $id);
        $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$this->CI->db->update('sr_admincontrol', $data);
	}
	
	public function remove_user($id)
	{
		$this->CI->db->query("DELETE FROM sr_admincontrol WHERE id = ? AND client_id = ?",array($id,$this->CI->session->userdata['logged_in']['client_id']));
		return true;
	}

}