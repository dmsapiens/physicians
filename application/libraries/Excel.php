<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Excel {

    private $excel;

    public function __construct() {
        require_once APPPATH . 'third_party/PHPExcel.php';
        $this->excel = new PHPExcel();
	}

    public function load($path) {
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $this->excel = $objReader->load($path);
    }

    public function save($path) {
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save($path);
    }

    public function stream($filename, $data = null, $filter = false) {
//echo "here";
        if ($data != null) {

			$reportlist=$data['reportlist'];
            
            if ($filter) {
                $rowNumber = 2;
                $filters = 'Timeframe: ' . ucfirst($filter['timeframe']).' Start Date: ' . $filter['fromdate'] . ' End Date: ' . $filter['todate'] . ' Rating: ' . $filter['rating'] . ' Location: ' . $filter['location'];
                $this->excel->getActiveSheet()->mergeCells('A1:G1');
                $this->excel->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			    $this->excel->getActiveSheet()->setCellValueExplicit('A1', $filters, PHPExcel_Cell_DataType::TYPE_STRING);
            } else {
                $rowNumber=1;
            }

			#$rowNumber=2;
			foreach ($reportlist as $key=>$row) {
				$col = 'A';
				//$rowNumber++;
				//$this->excel->getActiveSheet()->setCellValue('A' . $rowNumber, "No");
				$this->excel->getActiveSheet()->getStyle('A'.$rowNumber)->getFont()->setBold(true);
				foreach ($row as $key=>$val) {
				$objRichText = new PHPExcel_RichText();
				$objPayable = $objRichText->createTextRun(ucwords(str_replace("_", " ", $key)));
				$objPayable->getFont()->setBold(true)
										->getColor()->setRGB('545454');
				$this->excel->getActiveSheet()->getCell($col . $rowNumber)->setValue($objRichText);
				$this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
					$col++;
				}
			}
				$index=1;
				foreach ($reportlist as $key=>$row) {

				$rowNumber++;
				$col = 'A';
					//$this->excel->getActiveSheet()->setCellValue('A' . $rowNumber, $index);
					foreach ($row as $key => $val) {
					$this->excel->getActiveSheet()->getStyle($col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$this->excel->getActiveSheet()->setCellValueExplicit($col . $rowNumber, $val, PHPExcel_Cell_DataType::TYPE_STRING);
					//$this->excel->getActiveSheet()->setCellValue($col . $rowNumber, $val);
                    $col++;
					}

					$index++;
					$col = 'B';

				}
			}

		header('Content-type: application/ms-excel');
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Cache-control: private");
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save("export/$filename");
        header("location: " . base_url() . "export/$filename");
        unlink(base_url() . "export/$filename");
    }

	public function __call($name, $arguments) {
        if (method_exists($this->excel, $name)) {
            return call_user_func_array(array($this->excel, $name), $arguments);
        }
        return null;
    }
}